package org.parisnanterre.korector.integration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JenkinsConfig {

    @Value("${jenkins.credential.git-server}")
    private String credentialGitServer;

    @Value("${jenkins.host}")
    private String host;

    @Value("${jenkins.username}")
    private String username;

    @Value("${jenkins.password}")
    private String password;

    public String getCredentialGitServer() {
        return credentialGitServer;
    }

    public void setCredentialGitServer(String credentialGitServer) {
        this.credentialGitServer = credentialGitServer;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
