package org.parisnanterre.korector.integration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GitServerConfig {

    @Value("${gitServer.remote.username}")
    private String usernameRemote;

    @Value("${gitServer.remote.host}")
    private String hostRemote;

    @Value("${gitServer.remote.port}")
    private String port;

    @Value("${gitServer.remote.private-key.path}")
    private String keyPath;

    @Value("${gitServer.remote.private-key.passphrase}")
    private String passphrase;

    @Value("${gitServer.remote.path-repositories}")
    private String pathRepo;

    @Value("${gitServer.repository-url.ssh}")
    private String repoUrl;

    public String getUsernameRemote() {
        return usernameRemote;
    }

    public void setUsernameRemote(String usernameRemote) {
        this.usernameRemote = usernameRemote;
    }

    public String getHostRemote() {
        return hostRemote;
    }

    public void setHostRemote(String hostRemote) {
        this.hostRemote = hostRemote;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    public String getPathRepo() {
        return pathRepo;
    }

    public void setPathRepo(String pathRepo) {
        this.pathRepo = pathRepo;
    }

    public String getRepoUrl() {
        return repoUrl;
    }

    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }
}
