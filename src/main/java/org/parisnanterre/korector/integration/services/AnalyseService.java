package org.parisnanterre.korector.integration.services;

import org.parisnanterre.korector.integration.entity.Analyse;

import java.util.List;

public interface AnalyseService {
    List<Analyse> getAll();
    Analyse getOne(Long id);
    Analyse save(Analyse analyse);
    void delete(Long id);
    void lancementBuildJenkins(Analyse analyse, String bearerToken);
}
