package org.parisnanterre.korector.integration.services.impl;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.parisnanterre.korector.integration.config.GitServerConfig;
import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.parisnanterre.korector.integration.repository.AnalyseRepository;
import org.parisnanterre.korector.integration.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public class AnalyseServiceImpl implements AnalyseService {

    @Autowired
    private AnalyseRepository analyseRepository;
    @Autowired
    private CloneRepoService cloneRepoService;
    @Autowired
    private JenkinsService jenkinsService;
    @Autowired
    private SonarqubeService sonarqubeService;
    @Autowired
    private JschFileTransfer jschFileTransfer;
    @Autowired
    private GitServerConfig gitServerConfig;

    @Override
    public List<Analyse> getAll(){
        return analyseRepository.findAll();
    }

    @Override
    public Analyse getOne(Long id){
        return analyseRepository.getOne(id);
    }

    @Override
    public Analyse save(Analyse analyse){
        return analyseRepository.save(analyse);
    }

    @Override
    public void delete(Long id) {
        analyseRepository.deleteById(id);
    }

    @Async("taskExecutor")
    public void lancementBuildJenkins(Analyse analyse, String bearerToken){
        System.out.println("Execute method asynchronously - "
                + Thread.currentThread().getName());
        final long start = System.currentTimeMillis();
        try {
            String nameRepository = cloneRepoService.importProjectFromGithub(analyse.getName(),analyse.getRepository(),bearerToken);
            File file = new File(nameRepository);
            Session session = jschFileTransfer.sendFileToRemoteScp(nameRepository);
            JschFileTransferImpl.deleteFolder(file);
            String sshRepository = gitServerConfig.getRepoUrl() + nameRepository;
            //TODO: Conditionner si scanner activé
            analyse.setCommande(sonarqubeService.adaptCommandForSonarScanner(analyse));
            analyse = jenkinsService.lancerAnalyse(analyse,sshRepository);
            jschFileTransfer.deleteRemoteDirectory(session,file);

        } catch (GitAPIException e) {
            analyse.setResultat("Erreur lors du clonage du repository git : " + e.getMessage());
            JschFileTransferImpl.deleteFolder(new File(analyse.getName()));

        } catch (IOException | JSchException | InterruptedException e) {
            analyse.setResultat("Erreur lors de l'envoi du repository vers le git-server : " + e.getMessage());
            JschFileTransferImpl.deleteFolder(new File(analyse.getName()));
        }
        //TODO: Conditionner si scanner activé
        if("SUCCESS".equals(analyse.getResultat())) {
            lancementAnalyseSonar(analyse);
        }
        this.save(analyse);
        System.out.println("Elapsed time: "+ (System.currentTimeMillis() - start));
    }

    private void lancementAnalyseSonar(Analyse analyse){ //TODO: Externaliser les métriques
        try {
            Set<ResultatScanner> resultatScannerSet = sonarqubeService.getMetricsSonar("duplicated_lines_density,code_smells,sqale_rating,sqale_index,bugs,reliability_rating,vulnerabilities,security_rating,security_hotspots,branch_coverage,tests"
                    , analyse);
            analyse.setResultatScanner(resultatScannerSet);
        } catch (IOException | RuntimeException e) {
            analyse.setResultat(analyse.getResultat() + "Erreur lors de l'accès au scanner : " + e.getMessage());
        }
    }
}
