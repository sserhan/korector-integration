package org.parisnanterre.korector.integration.services.impl;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.parisnanterre.korector.integration.payload.response.ResultatScannerResponse;
import org.parisnanterre.korector.integration.repository.ResultatScannerRepository;
import org.parisnanterre.korector.integration.services.ResultatScannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultatScannerServiceImpl implements ResultatScannerService {

    @Autowired
    private ResultatScannerRepository resultatScannerRepository;

    @Override
    public List<ResultatScanner> getResultatScannerByAnalyse(Analyse analyse) {
        return resultatScannerRepository.findByAnalyse(analyse);
    }
}
