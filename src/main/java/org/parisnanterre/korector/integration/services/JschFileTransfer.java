package org.parisnanterre.korector.integration.services;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.IOException;

public interface JschFileTransfer {
    public Session sendFileToRemoteScp(String filename) throws IOException, JSchException, InterruptedException;
    public void deleteRemoteDirectory(Session session, File directory) throws JSchException, InterruptedException, IOException;
}
