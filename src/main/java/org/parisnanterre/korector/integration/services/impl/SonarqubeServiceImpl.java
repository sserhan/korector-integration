package org.parisnanterre.korector.integration.services.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.parisnanterre.korector.integration.config.SonarqubeConfig;
import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.parisnanterre.korector.integration.services.SonarqubeService;
import org.parisnanterre.korector.integration.util.BuildToolsEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

@Service
public class SonarqubeServiceImpl implements SonarqubeService {

    @Autowired
    private SonarqubeConfig sonarqubeConfig;

    @Override
    public Set<ResultatScanner> getMetricsSonar(String listMetrics, Analyse analyse) throws IOException {
        URL url = new URL(sonarqubeConfig.getUrl() + "/api/measures/component?metricKeys=" + listMetrics + "&component=" + analyse.getName() + "." + analyse.getId());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("hello", "application/json");
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP Error code : "
                    + conn.getResponseCode());
        }
        InputStreamReader in = new InputStreamReader(conn.getInputStream());
        BufferedReader br = new BufferedReader(in);
        String output = br.readLine();
        JsonObject js= new JsonParser().parse(output).getAsJsonObject();
        JsonArray metrics = js.getAsJsonObject("component").getAsJsonArray("measures");
        Set<ResultatScanner> resultatScanners = new HashSet<>();
        for(JsonElement metric : metrics){
            ResultatScanner resultatScanner = new ResultatScanner();
            resultatScanner.setNameMetric(metric.getAsJsonObject().get("metric").getAsString());
            resultatScanner.setValueMetric(metric.getAsJsonObject().get("value").getAsDouble());
            resultatScanner.setAnalyse(analyse);
            resultatScanners.add(resultatScanner);
        }
        conn.disconnect();
        return resultatScanners;
    }

    @Override
    public String adaptCommandForSonarScanner(Analyse analyse) {
        if(analyse.getBuildToolName().equals(BuildToolsEnum.MAVEN.getTool())){
            StringBuilder sb = new StringBuilder(analyse.getCommande());
            sb.append(" sonar:sonar")
                    .append(" -Dsonar.host.url=").append(sonarqubeConfig.getUrl())
                    .append(" -Dsonar.projectKey=").append(analyse.getName() + "." + analyse.getId());
            return sb.toString();
        }
        return analyse.getCommande();
    }
}
