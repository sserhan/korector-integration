package org.parisnanterre.korector.integration.services;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Set;

public interface SonarqubeService {
    Set<ResultatScanner> getMetricsSonar(String listMetrics, Analyse analyse) throws IOException;
    String adaptCommandForSonarScanner(Analyse analyse);
}
