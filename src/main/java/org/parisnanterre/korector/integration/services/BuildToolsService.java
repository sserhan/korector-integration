package org.parisnanterre.korector.integration.services;

import org.parisnanterre.korector.integration.entity.BuildTools;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BuildToolsService {

    List<BuildTools> all();

    BuildTools save(BuildTools buildTools);

    BuildTools getOne(Long id);

    void delete(Long id);


}
