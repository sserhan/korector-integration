package org.parisnanterre.korector.integration.services.impl;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.tools.ant.taskdefs.optional.ssh.ScpToMessage;
import org.parisnanterre.korector.integration.config.GitServerConfig;
import org.parisnanterre.korector.integration.services.JschFileTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Properties;

@Service
public class JschFileTransferImpl implements JschFileTransfer {
    @Autowired
    private GitServerConfig gitServerConfig;

    private static final int SHORT_WAIT_MSEC = 1000;

    private JschFileTransferImpl() {
        // prevent instantiation
    }

    /**
     * Envoi d'un fichier vers un remote server grâce à SCP.
     * @param filename nom du fichier à envoyer
     * @return la session de connexion ssh crée
     * @throws IOException on SCP operation failure
     * @throws JSchException on general SSH errors
     * @throws InterruptedException if creating directory failed
     */
    public Session sendFileToRemoteScp(String filename) throws IOException, JSchException, InterruptedException{
        Resource resource = new ClassPathResource("id_rsa");
        Session session = createSession(gitServerConfig.getUsernameRemote(),gitServerConfig.getHostRemote(),readFromInputStream(resource.getInputStream()),"".equals(gitServerConfig.getPassphrase()) ? null : gitServerConfig.getPassphrase());
        uploadDirectory(session,new File(filename),gitServerConfig.getPathRepo());
        return session;
    }

    /**
     * Lecture du contenu d'un InputStreaù
     * @param inputStream inputStream à lire
     * @return le contenu du stream sous la forme d'un String
     */
    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    /**
     * Création d'une session avec le remote server
     * @param user nom d'utilisateur
     * @param host ip ou DNS de l'hôte
     * @param keyFilePath chemin de la clef SSH privée
     * @param keyPassword passphrase de la clef SSH
     * @return la nouvelle session créée
     */
    private static Session createSession(String user, String host, String keyFilePath, String keyPassword) throws JSchException {
        JSch jsch = new JSch();
        if (keyFilePath != null) {
            if (keyPassword != null) {
                jsch.addIdentity("id_rsa",keyFilePath.getBytes(),null, keyPassword.getBytes());
            } else {
                jsch.addIdentity("id_rsa",keyFilePath.getBytes(),null, null);
            }
        }

        Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");

        Session session = jsch.getSession(user, host);
        session.setConfig(config);
        session.connect();

        return session;
    }

    /**
     * Uploads a file with SCP.
     *
     * @param session an established JSch session
     * @param localFile the local {@link File} to upload
     * @param remotePath the remote target path of the file to copy, using a relative path to the initial SSH "home" directory if necessary
     * @throws JSchException on general SSH errors
     * @throws IOException on SCP operation failure
     */
    private static void uploadFile(Session session, File localFile, String remotePath) throws IOException, JSchException {
        ScpToMessage message = new ScpToMessage(session, localFile, remotePath);
        message.execute();
    }

    /**
     * Uploads directories with SCP.
     *
     * @param session an established JSch session
     * @param directory the local directory to upload
     * @param remotePath the remote target path of the directory to copy, using a relative path to the
     *        initial SSH "home" directory if necessary
     * @throws JSchException on general SSH errors
     * @throws IOException on SCP operation failure
     * @throws InterruptedException if creating directory failed
     */
    private static void uploadDirectory(Session session, File directory, String remotePath)
            throws IOException, JSchException, InterruptedException {

        remotePath = remotePath + "/" + directory.getName();

        ChannelExec channel = (ChannelExec) session.openChannel("exec");
        // NOTE: the provided paths are expected to require no escaping
        channel.setCommand("mkdir -p " + remotePath);
        channel.connect();
        while (!channel.isClosed()) {
            // dir creation is usually fast, so only wait for a short time
            Thread.sleep(SHORT_WAIT_MSEC);
        }
        channel.disconnect();
        if (channel.getExitStatus() != 0) {
            throw new IOException("Creating directory failed: "  + remotePath);
        }

        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                uploadDirectory(session, file, remotePath);
            } else {
                uploadFile(session, file, remotePath);
            }
        }
    }

    /**
     * Suppression d'un répertoire sur le server remote
     * @param session la session SSH en cours avec le serveur
     * @param directory le dossier à supprimer
     * @throws JSchException on general SSH errors
     * @throws IOException on SCP operation failure
     * @throws InterruptedException if creating directory failed
     */
    public void deleteRemoteDirectory(Session session, File directory) throws JSchException, InterruptedException, IOException {
        String remotePath = gitServerConfig.getPathRepo();
        remotePath = remotePath + "/" + directory.getName();
        ChannelExec channel = (ChannelExec) session.openChannel("exec");
        channel.setCommand("rm -rf " + remotePath + "/");
        channel.connect();
        while (!channel.isClosed()) {
            // dir creation is usually fast, so only wait for a short time
            Thread.sleep(SHORT_WAIT_MSEC);
        }
        channel.disconnect();
        if (channel.getExitStatus() != 0) {
            throw new IOException("Delete directory failed: "  + remotePath);
        }
    }

    /**
     * Suppression d'un dossier localement
     * @param folder dossier à supprimer
     */
    public static void deleteFolder(File folder){
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }
}
