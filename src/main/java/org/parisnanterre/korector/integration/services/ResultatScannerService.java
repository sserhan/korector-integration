package org.parisnanterre.korector.integration.services;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;

import java.util.List;

public interface ResultatScannerService {
    List<ResultatScanner> getResultatScannerByAnalyse(Analyse analyse);
}
