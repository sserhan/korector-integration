package org.parisnanterre.korector.integration.services.impl;

import org.apache.commons.io.FileUtils;
import org.parisnanterre.korector.integration.services.ParseXmlService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.ParameterizedType;

public class ParseXmlServiceImpl implements ParseXmlService {

    private static final String JOB_MAVEN = "jobMaven.xml";

    @Override
    public String parseXml(String repository, String commande, String buildName, String branche, String gitCredential) throws IOException, SAXException, ParserConfigurationException, TransformerException {
        Document doc = getXmlDocument(JOB_MAVEN);
        switch (buildName){
            case "Maven":
                doc =  parseXmlMaven(doc,repository,commande, branche, gitCredential);
                break;
            default:
                return "";
        }
        return transfomeDocToString(doc);
    }

    /**
     * Ajoute les informations nécessaires au fonctionnement du fichier de configuration du xml
     * @param repository le chemin du repository sur lequel on souhaite effecter le job
     * @param commande la commande à éxecuter dans le job
     * @param gitCredential le nom des identifiants permmetant la connexion au repository
     * @return le fichier sous la forme d'un string.
     */
    private Document parseXmlMaven(Document doc,String repository, String commande, String branche, String gitCredential) throws IOException, SAXException, ParserConfigurationException, TransformerException {

            Node url = doc.getElementsByTagName("url").item(0);
            url.setTextContent(repository);
            Node credentialId = doc.getElementsByTagName("credentialsId").item(0);
            credentialId.setTextContent(gitCredential);
            Node targets = doc.getElementsByTagName("targets").item(0);
            targets.setTextContent(commande);
            Node gitBranch = doc.getElementsByTagName("name").item(0);
            gitBranch.setTextContent("*/"+branche);

            return doc;
    }

    /**
     * Récupère un document Xml stocké dans les ressources de l'application
     * @param fileName nom du document Xml
     * @return Renvoie le document Xml prêt à être parsé
     */
    private Document getXmlDocument(String fileName) throws ParserConfigurationException, IOException, SAXException {
        Resource resource = new ClassPathResource(fileName);
        InputStream inputStream = resource.getInputStream();
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        return docBuilder.parse(inputStream);
    }

    /**
     * Transforme le document en String
     * @param doc document à transformer
     * @return le contenu du document sous la forme d'un string.
     */
    private String transfomeDocToString(Document doc) throws TransformerException, IOException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        return writer.toString();
    }

}
