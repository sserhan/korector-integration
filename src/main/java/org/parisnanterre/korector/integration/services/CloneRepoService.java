package org.parisnanterre.korector.integration.services;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public interface CloneRepoService {
    boolean importProjectFromZip(String filePath);
    String importProjectFromGithub(String name,String URL,String token) throws GitAPIException;
}
