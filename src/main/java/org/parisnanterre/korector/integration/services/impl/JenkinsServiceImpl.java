package org.parisnanterre.korector.integration.services.impl;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.helper.BuildConsoleStreamListener;
import com.offbytwo.jenkins.model.BuildResult;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.parisnanterre.korector.integration.config.JenkinsConfig;
import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.services.AnalyseService;
import org.parisnanterre.korector.integration.services.JenkinsService;
import org.parisnanterre.korector.integration.util.BuildResultEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

@Service
public class JenkinsServiceImpl implements JenkinsService {

    private JenkinsServer jenkinsServer;
    private ParseXmlServiceImpl parseXmlService = new ParseXmlServiceImpl();
    private final JenkinsConfig jenkinsConfig;

    @Autowired
    private AnalyseService analyseService;

    public JenkinsServiceImpl(JenkinsConfig jenkinsConfig) throws URISyntaxException {
        this.jenkinsConfig = jenkinsConfig;
        initJenkinsServer(jenkinsConfig.getUsername(), jenkinsConfig.getPassword(), jenkinsConfig.getHost());
    }

    /**
     * Lancement d'un build Jenkins : crée un job, lance le build, supprime le job.
     * @param analyse Contient toutes les informations du projet ) analyser
     * @return l'analyse finie avec le résultat de celle-ci
     */
    @Override
    public Analyse lancerAnalyse(Analyse analyse, String sshRepository) {
        analyse.setResultat("EN COURS");
        analyse = analyseService.save(analyse);
        try {
            String job = createJob(analyse.getName(), parseXmlService.parseXml(sshRepository, analyse.getCommande(), analyse.getBuildToolName() , analyse.getBranche(), jenkinsConfig.getCredentialGitServer()));
            analyse.setResultat(buildJob(job, true,analyse));
            analyse.setCloning(true); // On l'ajoute ici et dans la fonction build pour mettre la bd au courant et ne pas changer sa valeur ici
            analyse.setOutput(getOutPut(job));
            deleteJob(job);
            analyse.setEnd(true);
        } catch (IOException | ParserConfigurationException | SAXException | TransformerException | InterruptedException e) {
            analyse.setResultat("Erreur lors de la création du job" + e.getMessage());
            analyse.setEnd(true);
            return analyse;
        }
        return analyse;
    }

    /**
     * Initialisation d'un serveur jenkins lancé
     * @param login username du jenkins
     * @param password password du jenkins
     * @param url url du jenkins
     */
    private void initJenkinsServer(String login, String password, String url) throws URISyntaxException {
        this.jenkinsServer = new JenkinsServer(new URI(url), login, password);
    }

    /**
     * Création d'un job dans jenkins
     * @param name nom du job souhaité
     * @param xml le fichier xml de config sous la forme d'un string
     * @return le nom du job créé
     */
    private String createJob(String name, String xml) throws IOException {
        jenkinsServer.createJob(name , xml,true);
        String job = jenkinsServer.getJob(name).getDisplayName();
        System.out.println(job);
        return job;
    }

    /**
     * Lance la compilation d'un job
     * @param name nom du job à compiler
     * @param isNewJob true c'est la première compilation du job
     * @return le résultat du job
     */
    private String buildJob(String name, boolean isNewJob, Analyse analyse) throws IOException, InterruptedException {
        jenkinsServer.getJob(name).build(true);
        analyse.setCloning(true);
        analyse = analyseService.save(analyse);
        if(this.waitForBuildToComplete(name,getLastBuildNumber(name) + 1,isNewJob))
            return this.getResultLasBuild(name);
        else
            return "TIME OUT";
    }

    /**
     * Attend que le build en cours soit terminé
     * @param jobName nom du job que l'on souhaite surveiller
     * @param numBuildExpected le numéro du build dont on attend la fin
     * @param isNewJob true c'est la première compilation du job
     * @return true lorsque le job est fini
     */
    private boolean waitForBuildToComplete(String jobName,int numBuildExpected,boolean isNewJob) throws InterruptedException, IOException {
        JobWithDetails wrkJobData = jenkinsServer.getJob(jobName);
        boolean buildCompleted = false;
        while (!buildCompleted) {
            Thread.sleep(10000);
            int numLastBuild = isNewJob ? 0 :  wrkJobData.getAllBuilds().get(0).getNumber();
            buildCompleted = numLastBuild == numBuildExpected && !wrkJobData.getAllBuilds().isEmpty()
                    && !wrkJobData.getAllBuilds().get(0).details().isBuilding();
        }
        return true;
    }

    /**
     * Renvoie le résultat du dernier build d'un job
     * @param jobName nom du job concerné
     * @return le résultat sour la forme d'un string (SUCCESS OR FAILURE)
     */
    private String getResultLasBuild(String jobName) throws IOException{
         return jenkinsServer.getJob(jobName).getLastBuild().details().getResult().name();
    }

    /**
     * Renvoie l'output du dernier build d'un job
     * @param jobName nom du job concerné
     * @return l'output sour la forme d'un string (ATTENTION: les outputs jenkins peuvent atteindre une très grande taille)
     */
    private String getOutPut(String jobName) throws IOException {
        return jenkinsServer.getJob(jobName).getLastBuild().details().getConsoleOutputHtml();
    }

    /**
     * Renvoie le numéro du dernier build d'un job
     * @param jobName nom du job concerné
     * @return un int correspondant au numéro du dernier build
     */
    private int getLastBuildNumber(String jobName) throws IOException {
        return jenkinsServer.getJob(jobName).getLastBuild().getNumber();
    }

    /**
     * Suppresion d'un job
     * @param name nom du job concerné
     */
    private void deleteJob(String name) throws IOException {
        if (jenkinsServer.getJob(name) != null) {
            jenkinsServer.deleteJob(name, true);
        }
    }
}
