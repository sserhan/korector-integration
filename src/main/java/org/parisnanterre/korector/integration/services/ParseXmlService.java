package org.parisnanterre.korector.integration.services;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public interface ParseXmlService {
    public String parseXml(String repository, String commande, String buildName, String branche, String gitCredential) throws IOException, SAXException, ParserConfigurationException, TransformerException;
}
