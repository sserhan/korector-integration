package org.parisnanterre.korector.integration.services.impl;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.parisnanterre.korector.integration.services.CloneRepoService;
import org.springframework.stereotype.Service;

import java.io.File;
@Service
public class CloneRepoServiceImpl implements CloneRepoService {

    //TODO: à implémenter
    @Override
    public boolean importProjectFromZip(String filePath) {
        return false;
    }

    /**
     * importe localment un projet depuis un git repository
     * @param name nom du dossier dans lequel sera sauvegardé le projet
     * @param URL lien de clonage du repository
     * @param token pour authentifier l'utilisateur
     * @return le nom du dossier importé
     * @throws GitAPIException Erreur provenant de l'API Git.
     */
    @Override
    public String importProjectFromGithub(String name,String URL,String token) throws GitAPIException {
        Git git = Git.cloneRepository()
                .setURI(URL) // correspond à l'attribut getHttpTransportUrl de l'api java github
                .setDirectory(new File(name))
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(token, ""))
                .call();
        String nameRepository = git.getRepository().getWorkTree().getName();
        git.close();
        return nameRepository;
    }
}