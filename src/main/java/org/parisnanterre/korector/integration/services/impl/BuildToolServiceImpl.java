package org.parisnanterre.korector.integration.services.impl;

import org.parisnanterre.korector.integration.entity.BuildTools;
import org.parisnanterre.korector.integration.repository.BuildRepository;
import org.parisnanterre.korector.integration.services.BuildToolsService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BuildToolServiceImpl implements BuildToolsService {

    private final BuildRepository buildRepository;

    public BuildToolServiceImpl(BuildRepository buildRepository) {
        this.buildRepository = buildRepository;
    }

    @Override
    public List<BuildTools> all() {
        return buildRepository.findAll();
    }

    @Override
    public BuildTools save(BuildTools buildTools) {
        return buildRepository.save(buildTools);
    }

    @Override
    public BuildTools getOne(Long id) {
        return buildRepository.getOne(id);
    }

    @Override
    public void delete(Long id) {
        buildRepository.deleteById(id);
    }
}
