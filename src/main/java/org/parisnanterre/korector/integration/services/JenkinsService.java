package org.parisnanterre.korector.integration.services;

import org.parisnanterre.korector.integration.entity.Analyse;

public interface JenkinsService {
    Analyse lancerAnalyse(Analyse analyse, String sshRepository);
}
