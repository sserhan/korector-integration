package org.parisnanterre.korector.integration.payload.request;

import java.io.Serializable;

public class AnalyseForm implements Serializable {
    private Long id;
    private String name;
    private String repository;
    private BuildToolsFormAnalyse build;
    private String branche;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public BuildToolsFormAnalyse getBuild() {
        return build;
    }

    public void setBuild(BuildToolsFormAnalyse build) {
        this.build = build;
    }


    public String getBranche() {
        return branche;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }
}