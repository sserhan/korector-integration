package org.parisnanterre.korector.integration.payload.response;

import java.util.Set;

public class BuildToolsResponse {

    private Long id;

    private String name;

    private Set<String> param;

    private String command;

    private String configFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getParam() {
        return param;
    }

    public void setParam(Set<String> param) {
        this.param = param;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
