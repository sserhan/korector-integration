package org.parisnanterre.korector.integration.payload;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.BuildTools;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.parisnanterre.korector.integration.payload.request.AnalyseForm;
import org.parisnanterre.korector.integration.payload.request.BuildToolsFormAnalyse;
import org.parisnanterre.korector.integration.payload.response.AnalyseResponse;
import org.parisnanterre.korector.integration.payload.response.BuildToolsResponse;
import org.parisnanterre.korector.integration.payload.response.ResultatScannerResponse;


public class ModelMapper {

    public static ResultatScannerResponse resultatScannerToResultatScannerResponse(ResultatScanner resultatScanner){
        ResultatScannerResponse resultatScannerResponse = new ResultatScannerResponse();
        resultatScannerResponse.setId(resultatScanner.getId());
        resultatScannerResponse.setNameMetric(resultatScanner.getNameMetric());
        resultatScannerResponse.setValueMetric(resultatScanner.getValueMetric());
        resultatScannerResponse.setAnalyse(analyseToAnalyseResponse(resultatScanner.getAnalyse()));
        return resultatScannerResponse;
    }

    public static AnalyseResponse analyseToAnalyseResponse(Analyse analyse) {
        AnalyseResponse analyseResponse = new AnalyseResponse();
        analyseResponse.setId(analyse.getId());
        analyseResponse.setOutput(analyse.getOutput());
        analyseResponse.setResultat(analyse.getResultat());
        analyseResponse.setRepository(analyse.getRepository());
        analyseResponse.setName(analyse.getName());
        analyseResponse.setCloning(analyse.isCloning());
        analyseResponse.setDateLancement(analyse.getDateLancement());
        analyseResponse.setBranche(analyse.getBranche());
        analyseResponse.setEnd(analyse.isEnd());
        return analyseResponse;
    }

    public static Analyse analyseFormToAnalyse(AnalyseForm analyseForm){
        Analyse analyse = new Analyse();
        analyse.setId(analyseForm.getId());
        analyse.setName(analyseForm.getName());
        analyse.setRepository(analyseForm.getRepository());
        analyse.setBranche(analyseForm.getBranche());
        analyse.setBuildToolName(analyseForm.getBuild().getName());
        analyse.setCommande(genererCommande(analyseForm.getBuild()));
        return analyse;
    }

    private static String genererCommande(BuildToolsFormAnalyse buildToolsFormAnalyse){
        // Utilisation d'un switch pour permettre l'ajout d'autres outils de builds : Graddle, Ant ...
        switch (buildToolsFormAnalyse.getName()){
            case "Maven":
                return genererMavenCommande(buildToolsFormAnalyse);
            default:
                return "";
        }
    }

    private static  String genererMavenCommande(BuildToolsFormAnalyse buildToolsFormAnalyse){
        StringBuilder commande;
        if (buildToolsFormAnalyse.getCommand() == null || "".equals(buildToolsFormAnalyse.getCommand())) {
            commande = new StringBuilder("clean install package");
            buildToolsFormAnalyse.getParams().forEach(s -> commande.append(" ").append(s));
        }else{
            commande = new StringBuilder(buildToolsFormAnalyse.getCommand());
        }
        return commande.toString();
    }

    public static BuildToolsResponse buildToolsToBuildToolsResponse(BuildTools buildTools){
        BuildToolsResponse buildToolsResponse = new BuildToolsResponse();
        buildToolsResponse.setId(buildTools.getId());
        buildToolsResponse.setName(buildTools.getName());
        buildToolsResponse.setCommand(buildTools.getCommand());
        buildToolsResponse.setConfigFile(buildTools.getConfigFile());
        buildToolsResponse.setParam(buildTools.getParams());
        return buildToolsResponse;
    }
}
