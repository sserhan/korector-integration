package org.parisnanterre.korector.integration.payload.request;

import java.io.Serializable;
import java.util.Set;

public class BuildToolsFormAnalyse implements Serializable {

    private String name;

    private String command;

    private Set<String> params;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Set<String> getParams() {
        return params;
    }

    public void setParams(Set<String> params) {
        this.params = params;
    }
}
