package org.parisnanterre.korector.integration.payload.response;

import org.parisnanterre.korector.integration.entity.Analyse;

import java.io.Serializable;

public class ResultatScannerResponse implements Serializable {
    private Long id;
    private String nameMetric;
    private Double valueMetric;
    private AnalyseResponse analyse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameMetric() {
        return nameMetric;
    }

    public void setNameMetric(String nameMetric) {
        this.nameMetric = nameMetric;
    }

    public Double getValueMetric() {
        return valueMetric;
    }

    public void setValueMetric(Double valueMetric) {
        this.valueMetric = valueMetric;
    }

    public AnalyseResponse getAnalyse() {
        return analyse;
    }

    public void setAnalyse(AnalyseResponse analyse) {
        this.analyse = analyse;
    }
}
