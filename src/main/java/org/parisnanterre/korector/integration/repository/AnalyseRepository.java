package org.parisnanterre.korector.integration.repository;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalyseRepository extends JpaRepository<Analyse,Long> {
}
