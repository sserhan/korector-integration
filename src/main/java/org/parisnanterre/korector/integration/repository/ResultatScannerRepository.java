package org.parisnanterre.korector.integration.repository;

import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultatScannerRepository extends JpaRepository<ResultatScanner,Long> {
    List<ResultatScanner> findByAnalyse(Analyse analyse);
}
