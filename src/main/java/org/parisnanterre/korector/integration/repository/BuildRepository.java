package org.parisnanterre.korector.integration.repository;

import org.parisnanterre.korector.integration.entity.BuildTools;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildRepository extends JpaRepository<BuildTools, Long> {
}
