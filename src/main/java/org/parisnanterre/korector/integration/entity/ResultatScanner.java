package org.parisnanterre.korector.integration.entity;

import javax.persistence.*;

@Entity
public class ResultatScanner {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nameMetric;
    private Double valueMetric;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "analyse_id")
    private Analyse analyse;

    public ResultatScanner() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameMetric() {
        return nameMetric;
    }

    public void setNameMetric(String nameMetric) {
        this.nameMetric = nameMetric;
    }

    public Double getValueMetric() {
        return valueMetric;
    }

    public void setValueMetric(Double valueMetric) {
        this.valueMetric = valueMetric;
    }

    public Analyse getAnalyse() {
        return analyse;
    }

    public void setAnalyse(Analyse analyse) {
        this.analyse = analyse;
    }
}
