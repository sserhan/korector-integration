package org.parisnanterre.korector.integration.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Analyse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Date dateLancement;
    private String repository;
    private String commande;
    private String buildToolName;
    private String resultat;
    @Column(length = 100000)
    private String output;
    @OneToMany(mappedBy = "analyse", cascade = CascadeType.ALL)
    private Set<ResultatScanner> resultatScanner;
    private String branche;
    private boolean cloning;
    private boolean end;

    public Analyse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateLancement() {
        return dateLancement;
    }

    public void setDateLancement(Date dateLancement) {
        this.dateLancement = dateLancement;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getCommande() {
        return commande;
    }

    public void setCommande(String commande) {
        this.commande = commande;
    }

    public String getBuildToolName() {
        return buildToolName;
    }

    public void setBuildToolName(String buildToolName) {
        this.buildToolName = buildToolName;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public Set<ResultatScanner> getResultatScanner() {
        return resultatScanner;
    }

    public void setResultatScanner(Set<ResultatScanner> resultatScanner) {
        this.resultatScanner = resultatScanner;
    }


    public String getBranche() {
        return branche;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }

    public boolean isCloning() {
        return cloning;
    }

    public void setCloning(boolean cloning) {
        this.cloning = cloning;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}
