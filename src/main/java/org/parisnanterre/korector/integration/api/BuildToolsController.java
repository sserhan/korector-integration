package org.parisnanterre.korector.integration.api;

import org.parisnanterre.korector.integration.payload.ModelMapper;
import org.parisnanterre.korector.integration.payload.response.BuildToolsResponse;
import org.parisnanterre.korector.integration.services.BuildToolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BuildToolsController {

    private final BuildToolsService buildToolsService;


    public BuildToolsController(BuildToolsService buildToolsService) {
        this.buildToolsService = buildToolsService;
    }
    @GetMapping("/buildTools")
    public ResponseEntity<List<BuildToolsResponse>> all(){
        List<BuildToolsResponse> buildToolsResponses = new ArrayList<>();
        buildToolsService.all().forEach(buildTools -> buildToolsResponses.add(
                ModelMapper.buildToolsToBuildToolsResponse(buildTools)
        ));
        return new ResponseEntity<>(buildToolsResponses, HttpStatus.OK);
    }

    @GetMapping("/buildTools/{id}")
    public ResponseEntity<BuildToolsResponse> one(@PathVariable Long id){
        return new ResponseEntity<>(ModelMapper.buildToolsToBuildToolsResponse(buildToolsService.getOne(id)), HttpStatus.OK);
    }
}
