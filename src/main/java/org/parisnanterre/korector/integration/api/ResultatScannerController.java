package org.parisnanterre.korector.integration.api;

import org.parisnanterre.korector.integration.payload.ModelMapper;
import org.parisnanterre.korector.integration.payload.response.ResultatScannerResponse;
import org.parisnanterre.korector.integration.services.AnalyseService;
import org.parisnanterre.korector.integration.services.ResultatScannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
public class ResultatScannerController {

    @Autowired
    ResultatScannerService resultatScannerService;
    @Autowired
    AnalyseService analyseService;

    @GetMapping("/resultatScanner/{id}")
    public List<ResultatScannerResponse> getResultatScannerByAnalyse(@PathVariable Long id){
        List<ResultatScannerResponse> resultatScannerResponses = new ArrayList<>();
        resultatScannerService.getResultatScannerByAnalyse(analyseService.getOne(id))
                .forEach(resultatScanner -> {
                    resultatScannerResponses.add(ModelMapper.resultatScannerToResultatScannerResponse(resultatScanner));
                });
        return resultatScannerResponses;
    }
}
