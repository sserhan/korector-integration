package org.parisnanterre.korector.integration.api;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.parisnanterre.korector.integration.config.GitServerConfig;
import org.parisnanterre.korector.integration.entity.Analyse;
import org.parisnanterre.korector.integration.entity.ResultatScanner;
import org.parisnanterre.korector.integration.payload.ModelMapper;
import org.parisnanterre.korector.integration.payload.request.AnalyseForm;
import org.parisnanterre.korector.integration.payload.response.AnalyseResponse;
import org.parisnanterre.korector.integration.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(path="/api/v1")
public class AnalyseController {

    @Autowired
    private JenkinsService jenkinsService;
    @Autowired
    private AnalyseService analyseService;

    @GetMapping("/analyses")
    public List<AnalyseResponse> getAll(){
        List<AnalyseResponse> analysesResponses = new ArrayList<>();
        analyseService.getAll().forEach(
                analyse -> analysesResponses.add(ModelMapper.analyseToAnalyseResponse(analyse))
        );
        return analysesResponses;
    }

    @GetMapping("/analyses/{id}")
    public AnalyseResponse getOne(@PathVariable Long id){
        return ModelMapper.analyseToAnalyseResponse(analyseService.getOne(id));
    }

    @PostMapping("/analyses")
    public CompletableFuture<AnalyseResponse> buildAndSave(@RequestHeader("Authorization") String bearerToken,@RequestBody AnalyseForm analyseForm)  {
        Analyse analyse =ModelMapper.analyseFormToAnalyse(analyseForm);
        analyse.setResultat("CLONING");
        analyse.setDateLancement(new Date());
        analyse = analyseService.save(analyse);
        System.out.println("Start Request");
        CompletableFuture<AnalyseResponse> analyseResponse = CompletableFuture.completedFuture(ModelMapper.analyseToAnalyseResponse(analyse));
        analyseService.lancementBuildJenkins(analyse,bearerToken);
        return analyseResponse;
    }

    @GetMapping("/analyse/relbuild/{id}")
    public CompletableFuture<AnalyseResponse> rebuildAnalyse(@RequestHeader("Authorization") String bearerToken, @PathVariable Long id){
        Analyse analyse = analyseService.getOne(id);
        analyse.setResultat("CLONING");
        analyse.setDateLancement(new Date());
        analyse.setEnd(false);
        analyse.setCloning(false);
        analyse = analyseService.save(analyse);
        CompletableFuture<AnalyseResponse> analyseResponse = CompletableFuture.completedFuture(ModelMapper.analyseToAnalyseResponse(analyse));
        analyseService.lancementBuildJenkins(analyse, bearerToken);
        return analyseResponse;
    }


    @DeleteMapping("/analyses/{id}")
    public void delete(@PathVariable Long id){
        this.analyseService.delete(id);
    }
}

