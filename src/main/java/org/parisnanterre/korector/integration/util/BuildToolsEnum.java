package org.parisnanterre.korector.integration.util;

public enum  BuildToolsEnum {
    MAVEN("Maven");

    private String tool;

    BuildToolsEnum(String tool){
        this.tool = tool;
    }

    public String getTool(){
        return this.tool;
    }
}
