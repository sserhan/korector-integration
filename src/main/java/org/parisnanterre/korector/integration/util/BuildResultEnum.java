package org.parisnanterre.korector.integration.util;

public enum BuildResultEnum {
    ABORTED("ABORTED"),
    BUILDING("BUILDING"),
    CANCELLED("CANCELLED"),
    FAILURE("FAILURE"),
    NOT_BUILT("NOT_BUILT"),
    REBUILDING("REBUILDING"),
    SUCCESS("SUCCESS"),
    UNKNOWN("UNKNOWN"),
    UNSTABLE("UNSTABLE");

    private String result;

    BuildResultEnum(String result){
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
