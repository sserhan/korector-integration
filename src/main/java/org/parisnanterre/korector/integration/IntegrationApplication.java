package org.parisnanterre.korector.integration;

import org.parisnanterre.korector.integration.entity.BuildTools;
import org.parisnanterre.korector.integration.services.BuildToolsService;
import org.parisnanterre.korector.integration.util.BuildToolsEnum;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@EnableEurekaClient
public class IntegrationApplication {
	public IntegrationApplication(BuildToolsService buildToolsService) {
		IntegrationApplication.buildToolsService = buildToolsService;
	}

	public static void main(String[] args) {
		SpringApplication.run(IntegrationApplication.class, args);
	}

	private static  BuildToolsService buildToolsService;

	@PostConstruct
	public static void init(){
		BuildTools maven = new BuildTools();
		maven.setName(BuildToolsEnum.MAVEN.getTool());
		maven.setConfigFile("jobMaven.xml");
		Set<String> mavenParam = new HashSet<>();
		mavenParam.add("-DskipTests");
		mavenParam.add("-Dskip.npm");
		mavenParam.add("-Ddockerfile.skip");
		maven.setParams(mavenParam);
		maven.setCommand("clean package install");
		maven = buildToolsService.save(maven);
	}

}
